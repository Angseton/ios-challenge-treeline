//
//  ListRouter.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation
import UIKit

protocol ListRouter: AnyObject {
    func routeToDetails(objectId: String)
}

class ListDefaultRouter: ListRouter {

    private weak var viewController: ListDefaultViewController!

    public init(viewController: ListDefaultViewController) {
        self.viewController = viewController
    }

    func routeToDetails(objectId: String) {
        let detailsViewController = DetailsDefaultViewController.build(objectId: objectId)
        viewController.navigationController?.pushViewController(detailsViewController, animated: true)
    }
}
